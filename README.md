# Virtual Piano #

This is a small sample app of a virtual piano, made with HTML, CSS and JavaScript. 

You are free to reuse or extend on this, as long as the original author (me) is mentioned. Please feel free to report any issues, 
talk about the project or ask questions, opening an issue.

Note: This project has been finished and will not be receiving any further development!